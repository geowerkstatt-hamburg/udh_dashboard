Repository für den Elastic Search Index udh_dashboard. Der Index wird im [Urban Data Platform Cockpit](https://geoportal-hamburg.de/udp-cockpit) genutzt.

Zurzeit ist nur die Vorlage für das dort genutzte Search Template udpc_query (udpc für Urban Data Platform Cockpit) in diesem Repo. 
Um das Search Template udpc_query und weitere Queries (zur Ausführung in den Kibana Dev Tools) und URLs zu erzeugen, das Skript transform.js mit Node.js ausführen. Weitere Dokumentation im Skript selbst. 

```javascript
node transform.js
```
Erzeugte Dateien werden in den Ordner /output geschrieben; der Ordner mit den aktuell erzeugten Dateien ist jetzt Teil dieses Repos:

- udpc_query_misc_kibana.json: verschiedene Queries zur Ausführung in den Kibana Dev Tools
- udpc_query_script.json: JSON-Objekt zum Anlegen des Search Templates über die Kibana Dev Tools
- udpc_query_urls.json: verschiedene URLs für das angelegte Search Template
