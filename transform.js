'use strict';

/*
Script that transforms a JSON object directed at an elastic _search/template endpoint 
to a JSON object that creates the corresponding search template against the _scripts endpoint.
The term filters containing the strings defined in the variable search_strs are made conditional and accepting arrays.
Also, several other JSON objects for use in the Kibana Dev Tools and URLs are written to /output.
*/

const fs = require('fs');
const path = require('path');
const args = process.argv.slice(2);


const transform = function (file) {

    const raw_data = fs.readFileSync(file);
    const template_id = path.basename(file, ".json");
    const search_strs = ["{{theme}}", "{{org}}", "{{tag}}", "{{status}}", "{{tag_not}}"];
    
    //remove first line : GET /...
    let string_data = raw_data.toString(); // stringify buffer
    var position = string_data.toString().indexOf('\n'); // find position of first new line element
    if (position != -1) { // if new line element found
        string_data = string_data.substr(position + 1); // subtract string based on first line length
    }
    const json_obj = JSON.parse(string_data);

    //use slice() to copy the array by value, not by reference
    let clauses = json_obj.source.query.bool.filter.slice();
    
    //currently there is only one clause in must_not
    if (json_obj.source.query.bool.must_not){
        clauses.push(json_obj.source.query.bool.must_not[0]);
    }

    let json_obj_source_str = JSON.stringify(json_obj.source);
    let script, template, source;

    //make clauses conditional and make them accept arrays using {{#toJson}}, if they include one of the search strings
    for (let i = 0; i < clauses.length; i++) {
            let clause_str = JSON.stringify(clauses[i]);                
            
            for (let k = 0; k < search_strs.length; k++) {
                const search_str = search_strs[k];
                const search_str_no_braces = search_str.replace("{{", "").replace("}}", "") ;
                
                if (clause_str.includes(search_str)) {
                    //currently there is only one clause in must_not (tag_not), thus not adding a comma 
                    if (search_str !== "{{tag_not}}"){
                        clause_str += ",";
                    }
                    
                    //add #toJson to clause_str
                    let replace = clause_str.replace("\"" + search_str + "\"", "{{#toJson}}" + search_str_no_braces + "{{/toJson}}");                        
                    //replace term filter to terms filter so arrays can be passed
                    replace = replace.replace("term", "terms");
                    replace = "{{#" + search_str_no_braces + ".0}}" + replace + "{{/" + search_str_no_braces + ".0}}";
                    json_obj_source_str = json_obj_source_str.replace(clause_str, replace);
                }
            }
    }
    //create output folder if needed
    if (!fs.existsSync("./output")){
        fs.mkdirSync("./output");
    }

    //construct JSON object to create the search template from against _scripts endpoint
    script = {
        "script": {
            "lang": "mustache",
            "source": json_obj_source_str
        }
    };
    fs.writeFileSync("./output/" + template_id + "_script.json", "POST _scripts/"+ template_id + "\n" +  JSON.stringify(script, null, 2));

    //construct miscellaneous other queries and urls and write them to file
    let file_misc_kibana = template_id + "_misc_kibana.json";
    let file_urls = template_id + "_urls.json";
    let _search = "GET udh_dashboard/_search/template";
    let _render = "GET /_render/template";
    
    //transform params that match one of the search strings from string to [string]
    for (let i = 0; i < search_strs.length; i++) {
        const el = search_strs[i];
        const param = el.replace("{{", "").replace("}}", "") ;
        if (json_obj.params[param]){
            let arr = [json_obj.params[param]];
            json_obj.params[param] = arr;
        }
    }

    template = {
        "source": json_obj_source_str,
        "params": json_obj.params
    }
    fs.writeFileSync("./output/" + file_misc_kibana, _search + "\n" + JSON.stringify(template, null, 2));
    fs.appendFileSync("./output/" + file_misc_kibana, "\n\n" + _render + "\n" + JSON.stringify(template, null, 2));

    source = {
        "id": template_id,
        "params": json_obj.params
    };
    fs.appendFileSync("./output/" + file_misc_kibana, "\n\n" + _search + "\n" + JSON.stringify(source, null, 2));
    fs.appendFileSync("./output/" + file_misc_kibana, "\n\n" + _render + "\n" + JSON.stringify(source, null, 2));
    
    fs.appendFileSync("./output/" + file_misc_kibana, "\n\nGET _scripts/" + template_id );

    let base_url = "https://api.hamburg.de/udh_dashboard";
    let query_string = "?source_content_type=application/json&source=";
    let url = base_url + query_string;

    fs.writeFileSync("./output/" + file_urls, _search + query_string + encodeURIComponent(JSON.stringify(source)));
    fs.appendFileSync("./output/" + file_urls, "\n\n" + url + encodeURIComponent(JSON.stringify(source)));
    fs.appendFileSync("./output/" + file_urls, "\n\n" + url + JSON.stringify(source));
};

//either take single file specified via cmd line
if (args.length > 0) {
    let file = args[0];
    transform(file);
} 
//or transform every json file in folder ./search_template
else {
    const dirPath = "./search_template";

    fs.readdir(dirPath, function (err, files) {
        //handling error
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        } 
        //iterate files
        files.forEach(function (file) {
            if (file.includes("json")) {
                transform(path.join(dirPath, file));
            }
        });
    });
};